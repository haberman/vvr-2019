#!/usr/bin/env python3

import statistics
import spidev
import time
import os

from datetime import timedelta, datetime

THRESHOLD_SAMPLES_COUNT = 50
STROBES_COUNT = 4

spi = None
threshold = None


def read_channel(channel):
    assert spi, threshold

    adc = spi.xfer2([1, (8+channel) << 4, 0])
    data = ((adc[1] & 3) << 8) + adc[2]

    return data


def read_samples(channel_range, sample_amount, sleep_time=.01):
    samples = []
    while sample_amount > 0:
        for c in channel_range:
            samples.append(read_channel(c))

        sample_amount -= 1
        time.sleep(sleep_time)

    return statistics.mean(samples)


def startup():
    global spi, threshold

    spi = spidev.SpiDev()
    spi.open(0, 0)
    spi.max_speed_hz = 1000000

    channel_range = range(0, STROBES_COUNT)
    threshold = read_samples(channel_range, THRESHOLD_SAMPLES_COUNT) * 1.1


def collect_reads(channel_range, duration=5):
    now = datetime.now()
    end = now + timedelta(seconds=duration)

    print('collecting {}s of data'.format(duration))

    reads = []
    while now < end:
        # strobe_states = [0 for c in channel_range]
        # channel_reads = []
        # r = [0 for c in channel_range]
        channel_reads = [0 for c in channel_range]
        for c in channel_range:
            read = read_channel(c)
            channel_reads[c] = read
        reads.append(channel_reads)
        time.sleep(.001)
        now = datetime.now()

    return reads
