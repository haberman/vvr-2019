# -*- coding:utf-8 -*-

import re
import twitter
import json

from urllib.parse import quote

from . import sqlite
from . import private

from poemade import guess_form
from poemade.tokenizer import tokenize
from poemade.parser import to_cp1252


QUERY_CREATE_HISTORY = '''CREATE TABLE IF NOT EXISTS history
                          (history_id INTEGER PRIMARY KEY AUTOINCREMENT,
                           history_type TEXT NOT NULL,
                           history_ceated INTEGER NOT NULL,
                           history_src_stzidx INTEGER NOT NULL,
                           history_res_id INTEGER NOT NULL,
                           history_res_stzidx INTEGER DEFAULT NULL)'''

QUERY_CREATE_STATUSES = '''CREATE TABLE IF NOT EXISTS statuses
                           (status_id INTEGER PRIMARY KEY,
                            status_author TEXT,
                            status_content TEXT,
                            status_hashtags TEXT,
                            status_info TEXT)'''

RE_URL = re.compile(
    r'(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?')

RE_HASHTAG = re.compile(r'(#[\w\d]+\s*)')
RE_MENTION = re.compile(r'(@[a-zA-Z0-9_]+\s*)')

res_folder = None
res_listing = None
twitter = twitter.Api(private.consumer_key, private.consumer_secret,
                      private.access_token, private.access_token_secret,
                      tweet_mode='extended')


def db_init(data_folder):
    global res_foler

    res_foler = '{}/res'.format(data_folder)
    db_file = '{}/idvi.db'.format(data_folder)
    return sqlite.Db(db_file, [QUERY_CREATE_HISTORY, QUERY_CREATE_STATUSES])


def twitter_statuses_fmt(statuses, min_char):
    '''
    Formats a list of raw `Status` into a (`id`,`author`,`text`,`hashtags`) tuple ready for sql insert.'''
    s_res = []

    def twitter_word(word):
        '''
        Tests if word contains substrings related to the twitter platform itself.
        Instead of a bool, this method will return the value of an hashtag of one is detected.'''
        if word.startswith('#'):
            return word
        elif word == '' or re.match(r'^\s*\(\w+\)\s*$', word):
            return True
        elif sum([1 for ch in word if ch.isalpha() or ch.isspace()]) < len(word)*.5:
            return True

        return False

    def strip_tokens(tokens, hashtags):
        '''
        Reverse lookup on tokens to removes words and/or lines.
        This method tempers its input by either deleting `tokens` or appending `hashtags`.'''
        for i in range(len(tokens)-1, -1, -1):
            words = tokens[i]
            if type(words) is tuple:
                words = words[1]

            word = twitter_word(words[-1])
            while len(words) > 0 and word is not False:
                if type(word) is str:
                    hashtags.add(word)

                del words[-1]
                try:
                    word = twitter_word(words[-1])
                except IndexError:
                    break

            if len(words) == 0:
                del tokens[i]
    for status in statuses:
        st_text = status.full_text
        if status.retweeted_status:
            st_text = status.retweeted_status.full_text
        elif status.quoted_status:
            st_text = status.quoted_status.full_text

        st_text = RE_URL.sub('', st_text).strip()
        st_text = RE_MENTION.sub('', st_text).strip()

        st_tokens = [w for w in [l.strip().split(' ') for l in st_text.splitlines()]]
        st_hashtags = set()
        strip_tokens(st_tokens, st_hashtags)

        enum_tokens = [(k, v) for k, v in enumerate(st_tokens)]
        st_tokens = sorted(enum_tokens, key=lambda x: x[0], reverse=True)
        strip_tokens(st_tokens, st_hashtags)

        st_lines = [' '.join(l[1]) for l in sorted(st_tokens, key=lambda x: x[0])]
        st_text = '\n'.join(st_lines)
        if len(st_text) <= min_char:
            continue

        st_text=to_cp1252(st_text)
        if not st_text:
            continue

        s_form=guess_form(tokenize(st_text))
        del s_form['stanza']
        del s_form['metric']

        s_res.append([status.id, status.user.screen_name,
                      st_text, json.dumps(s_form), ','.join(st_hashtags)])

    return s_res


def twitter_search(q, lang, min_char):
    q='q={}&lang={}&result_type=recent&count=20'.format(quote(q), lang)
    statuses=twitter.GetSearch(raw_query = q)

    return twitter_statuses_fmt(statuses, min_char)
