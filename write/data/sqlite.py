# -*- coding:utf-8 -*-

import os
import sqlite3
import re


class Db:
    def __init__(self, database, queries_init=None):
        self.database = database
        self.queries_init = queries_init

    def _create(self):
        open(self.database, 'a').close()

        if self.queries_init is not None:
            self.conn = sqlite3.connect(self.database)
            self.cursor = self.conn.cursor()
            self.cursor.executescript(';\n'.join(self.queries_init))
            self.close()

    def connect(self):
        if not os.path.isfile(self.database):
            self._create()

        self.conn = sqlite3.connect(self.database, detect_types=sqlite3.PARSE_COLNAMES)
        self.cursor = self.conn.cursor()

    def close(self):
        self.conn.commit()
        self.conn.close()

    def count(self, table='history'):
        self.connect()
        self.cursor.execute('SELECT COUNT(*) FROM {table}'.format(table))

        result = self.cursor.fetchone()

        if result:
            result = result[0]

        self.close()
        return result

    def fetch_one(self, table, id):
        query = 'SELECT * FROM {} WHERE {}_id = ?'.format(table, table[:-1])
        self.connect()
        self.cursor.execute(query, (id,))

        res = self.cursor.fetchone()
        self.close()
        return res

    def fetch_all(self, table='history', sort_by=None, order=None, limit=None):
        ''' Returns all rows of given table. '''

        self.connect()

        query = 'SELECT * FROM {}'.format(table)
        if sort_by and order:
            query += 'ORDER BY {} {}'.format(sort_by, order)
        elif sort_by:
            query += 'ORDER BY {}'.format(sort_by)
        if limit:
            query += '{} LIMIT {}'.format(query, limit)

        self.cursor.execute(query)

        res = self.cursor.fetchall()
        self.close()
        return res

    def fetch_cols(self, table='history', cols=['history_id']):
        self.connect()

        query = 'SELECT {} FROM {}'.format(','.join(cols), table)
        self.cursor.execute(query)

        res = self.cursor.fetchall()
        self.close()
        return res

    def add_one(self, table, value, rows=None):
        query = 'INSERT INTO {} '.format(table)
        if rows:
            assert len(value) == len(rows)
            query += '({}) '.format(','.join(rows))
        ph = '?,' * len(value)
        query += 'VALUES({})'.format(ph[:-1])

        return self.execute(query, value)

    def add_multiple(self, table, values, rows=None, or_ignore=True):
        query = 'INSERT {}INTO {} '.format('OR IGNORE ' if or_ignore else ' ', table)
        if rows:
            query += '({}) '.format(','.join(rows))

        vals = []
        query += 'VALUES '
        for value in values:
            ph = '?,' * len(value)
            query += '({}),'.format(ph[:-1])
            vals.extend(value)

        return self.execute(query[:-1], vals)

    def update_one(self, where, where_id, value, table='history'):
        self.connect()
        self.cursor.execute('UPDATE {} SET {}=? WHERE {}=?'.format(table, value[0], where), (value[1], where_id))
        self.close()

    def execute(self, query, value):
        self.connect()

        if type(value) is not tuple:
            value = tuple(value)

        try:
            with self.conn:
                self.conn.execute(query, value)
        except sqlite3.IntegrityError as e:
            return 'sqlite error: {}'.format(e.args[0])

        except KeyError as e:
            return 'missing column error: {}'.format(e.args[0])

        finally:
            self.close()

        return True

    def delete_one(self, where, where_id, table='history'):
        self.connect()
        self.cursor.execute('DELETE FROM {} WHERE {}=?'.format(table, where), (where_id,))
        self.close()

    def delete_all(self, table='history'):
        ''' Used for testing '''

        self.connect()
        self.cursor.execute('DELETE FROM {}'.format(table))
        self.close()
