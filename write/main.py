#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Nothing that smart but fine tuned old school combinatory tricks for xgenerating poetry.
# We have an `src` (a poem from the telegraphic literature) and a fatass amount of poems called `res`.
# The goal is to make `res` kinda dance, poetically speaking, with `src`
# yep, that's pretty much it

import os
import sys
import json
import datetime
import random
import signal
import asyncio
import time

from transitions import Machine
from concurrent.futures import ProcessPoolExecutor

# from chip import mcp23s17
from data import db_init, twitter_search
from poemade.generator import poems_rand_load, stanza_sort, combine_gutenberg, combine_twitter
from poemade.parser import VERSE_MAX, poetry_checks, to_cp1252
from poemade import cmu_load

PROG_NAME = 'IDVI-write'
PATH_DATA_FOLDER = os.path.join(os.path.dirname(__file__), '../data')
PATH_SRC_FILE = '{}/src/nlpoetry_corpus.json'.format(PATH_DATA_FOLDER)
# PATH_RES_DIR = '{}/res'.format(PATH_DATA_FOLDER)
PATH_RES_DIR = os.path.join(os.path.dirname(__file__), '../../yaga/data/json')
PATH_CMU_DICT = '{}/cmudict-0.7b.json'.format(PATH_DATA_FOLDER)


def cbk_prepare():
    task = executor.submit(prepare)
    src_stzidx, res_data = task.result()
    fsm.encode(src_stzidx, res_data)


def cbk_encode(src_stzidx, res_data):
    c_verses, s_verses = [], []
    if len(res_data) == 2:
        c_verses, s_verses = res_data[1][2].split('\n'), res_data[0]
        db.add_one('history', [None, 'status', int(time.time()), src_stzidx, res_data[1][0], None])
    elif len(res_data) == 4:
        c_verses, s_verses = res_data[2], res_data[3]
        db.add_one('history', [None, 'stanza', int(time.time()), src_stzidx, res_data[0], res_data[1]])

    print('\ncbk_encode______________{}\n{}'.format(c_verses, s_verses))
    c_verses, s_verses = to_cp1252(c_verses), to_cp1252(s_verses)

    if not c_verses or not s_verses:
        print('can\'t encode {}->{}'.format(s_verses, c_verses))

    assert fsm
    fsm.release()


def cbk_release():
    poems = twitter_search('#micropoetry OR #poem', 'en', VERSE_MAX)

    print('Inserting {} statuses'.format(len(poems)))
    db_rows = ['status_id', 'status_author', 'status_content', 'status_info', 'status_hashtags']
    db.add_multiple('statuses', poems, rows=db_rows)


def prepare():
    def validate_candidate(candidate):
        lines_count = len(candidate[1][2])

        score = 0

        for l in candidate[1][2]:
            line_score = tests_count
            for i in poetry_checks.items():
                if not i[1](l.strip()):
                    line_score -= 1
            score += line_score

        if (score/tests_count) / lines_count > .9:
            return candidate
        else:
            return None

    history_idxs = db.fetch_cols('history', ['history_src_stzidx'])
    used_idxs = set([r[0] for r in history_idxs]) if len(history_idxs) > 0 else set()
    rand_idx = random.randint(0, len(sources)-1)

    if rand_idx in used_idxs and 0 < len(history_idxs) < len(sources):
        # return False
        return prepare()
    elif len(history_idxs) == len(sources):
        # return False
        db.delete_all()
        return prepare()

    src = sources[rand_idx]
    if len(src['verses']) <= 2:  # TODO -> @incomplete
        sess_data = combine_twitter(src, db.fetch_all('statuses'))
        print(sess_data)
    else:
        poems_rand = poems_rand_load(db, PATH_RES_DIR)
        print('-> {} random poems'.format(len(poems_rand)))

        stzs_sorted = stanza_sort(src, poems_rand)
        tests_count = len(poetry_checks.items())

        valid_candidates = []
        for stz in stzs_sorted:
            if validate_candidate(stz):
                valid_candidates.append((stz[1], stz[2]))

        sess_data = combine_gutenberg(src, valid_candidates)

    return rand_idx, sess_data


def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')

    executor.shutdown(True)
    sys.exit(0)


if __name__ == '__main__':
    states = ['idle', 'preparing', 'encoding']
    transitions = [{'trigger': 'prepare', 'source': 'idle', 'dest': 'preparing', 'after': cbk_prepare},
                   {'trigger': 'encode', 'source': 'preparing', 'dest': 'encoding', 'after': cbk_encode},
                   {'trigger': 'release', 'source': 'encoding', 'dest': 'preparing', 'after': cbk_release}]

    fsm = Machine(model='self', states=states, transitions=transitions, initial='idle')

    executor = ProcessPoolExecutor(max_workers=3)
    cmu_load(PATH_CMU_DICT)

    db = db_init(PATH_DATA_FOLDER)
    db.connect()

    # signal.signal(signal.SIGINT, signal_handler)

    with open(PATH_SRC_FILE) as json_handler:
        sources = json.load(json_handler)
        fsm.prepare()
