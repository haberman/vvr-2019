#!/usr/bin/env python
# -*- coding:utf-8 -*-

import argparse
import json
import re
import sys

CMUDICT = '../data/cmudict-0.7b'


# extract the word from entries with a parenthetical numeric suffix,
#   e.g. 'ACCENT' from 'ACCENT(1)'
def extract_word(word):
    match = re.match('(.*)\\([0-9]+\\)', word)
    return match.group(1) if match else word


if '__main__' == __name__:

    with open('{}.txt'.format(CMUDICT), encoding='ISO-8859-1') as txt_file:

        word_pronunciations = {}
        line = txt_file.readline()

        while line:
            # strip leading and trailing whitespace
            line = line.strip()

            # skip comments
            if line.startswith(';;;'):
                line = txt_file.readline()
                continue

            # split the line into tokens
            tokens = line.split()

            # extract the word from the first token
            word = extract_word(tokens[0]).lower()

            # extract the phonemes from the remaining tokens
            phonemes = tokens[1:]

            # add the phonemes to the list of pronunciations stored at word_pronunciations[word]
            word_pronunciations.setdefault(word, list()).append(phonemes)

            line = txt_file.readline()

    with open('{}.json'.format(CMUDICT), 'w') as json_file:
        json.dump(word_pronunciations, json_file, indent=2, sort_keys=True)
