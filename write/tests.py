#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import json
import time

from transitions import Machine
from functools import reduce

from poemade.parser import poetry_checks, is_poem, tokenize
from poemade import cmu_load, get_syllables

fsm = None


PATH_RES_DIR = os.path.join(os.path.dirname(__file__), '../../yaga/data/json')


def after_state_change():
    assert fsm
    print(fsm.state)


def test_transitions():
    # The states
    states = ['preparing', 'encoding', 'terminate']

    # And some transitions between states. We're lazy, so we'll leave out
    # the inverse phase transitions (freezing, condensation, etc.).

    transitions = [
        {'trigger': 'stream', 'source': 'preparing', 'dest': 'encoding'},
        {'trigger': 'release', 'source': 'encoding', 'dest': 'terminate'},
    ]

    global fsm
    fsm = Machine(model='self', states=states, transitions=transitions,
                  initial='preparing', after_state_change=after_state_change)
    # print(fsm)

    fsm.stream()
    fsm.release()


def test_pgid_stzidx(pgid, stzidx):
    with open('{}/pg{}-poems.json'.format(PATH_RES_DIR, pgid)) as fp:
        return json.load(fp)[stzidx]['verses']


def test_cmu_annotate():
    poem = '''    BENEATH the Vines, Hotel Belle Vue,
    I'm very certain I know who
      Here loves to trifle, I'm afraid,
      Or lounge upon the balustrade,
    And watch the Lake's oft changing hue.

    'Tis sweet to dream the morning through,
    While idle fancies we pursue,
      To pleasant plash of passing blade--
          Beneath the Vines!

    I love to laze; it's very true,
    I love the sky's supernal blue;
      To sit and smoke here in the shade,
      And slake my thirst with lemonade,
    And dream away an hour or two--
          Beneath the Vines!'''

    cmu_load('../data/cmudict-0.7b.json')

    tokens = tokenize(poem)

    cmu_form = []
    for line in tokens:
        cmu_lines = []
        for word in line:
            cmu_lines.append(''.join(get_syllables(word)[0]))
        cmu_form.append(' '.join(cmu_lines))

    print(','.join(cmu_form))

    # print(tokens)


def test_poetry_checks(test_str):
    return {k: f(test_str) for k, f in poetry_checks.items()}


def test_poetry_estimation():
    print(test_poetry_checks('       *       *       *       *       *'))
    print(is_poem(['       *       *       *       *       *', '', '              JOHNNY CROW\'S GARDEN', '',
                   '       *       *       *       *       *', '', '               JOHNNY CROW\'S PARTY', '',
                   '       *       *       *       *       *']))


def test_mpc():
    from chip import strobe_linear, strobe_random
    strobe_random()
    strobe_linear()


def test_twitter():
    from data import twitter_search
    print(twitter_search('%23poem', 'en', 66))


def test_db_st_cleanup():
    import re
    from data import db_init

    REP = [(u'\u200b', '')]

    db = db_init('../data')
    # statuses = db.fetch_all('statuses')
    for s in db.fetch_all('statuses'):
        if s[2].startswith('#'):
            print(s)
            db.delete_one('status_id', s[0], 'statuses')
            continue

        for l in s[2].splitlines():

            # l.replace(line)
            try:
                (w.encode('cp1252') for w in l.split(' '))
            except UnicodeEncodeError:
                print('anything ???')
                rep = dict((r[0], r[1]) for r in REP)
                pattern = re.compile("|".join(rep.keys()), re.UNICODE)
                text = pattern.sub(lambda m: rep[m.group(0)], l)
                # where, where_id, value, table='history'
                db.update_one('status_id', s[0], ('status_content', text), 'statuses')
                # db.delete_one('status_id', s[0], 'statuses')
                break


if __name__ == '__main__':
    test_db_st_cleanup()
    # test_twitter()
    # test_mpc()
    # test_cmu_annotate()
    # print(test_pgid_stzidx(22032, 18))
    # test_transitions()
