#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import logging
import json

from poemade.tokenizer import tokenize
from poemade import cmu_load, guess_form

PROG_NAME = 'NLPoetry Tagger'
SRC_FOLDER = '../data/src'
CMU_DICT = '../data/cmudict-0.7b.json'

if __name__ == '__main__':
    cmu_load(CMU_DICT)

    poems_ = []
    verses_count = 0
    for f in os.listdir(SRC_FOLDER):
        if f[-4:] != 'json' or '_poetry' not in f:
            continue

        logging.info('Parsing {}'.format(f))

        verses_count = 0
        with open(os.path.join(SRC_FOLDER, f)) as json_handler:
            poems = json.load(json_handler)

            for poem in poems:
                verses_count += len(poem['verses'])
                concat_poem = '\n'.join(poem['verses'])
                tokenized_poem = tokenize(concat_poem)

                data = guess_form(tokenized_poem)
                data['verses'] = poem['verses']

                poems_.append(data)

        with open('{}/nlpoetry_corpus.json'.format(SRC_FOLDER), 'w') as out:
            out.write(json.dumps(poems_))

    logging.info('Parsed {} poems ({} verses)'.format(len(poems), verses_count))
